//==================================================================
$(window).resize(function(event){
	adaptiveFunction();
	}
);

function adaptiveHeader(w,h) {
	let headerMenu=$('.menu__body');
	let headerLink=$('.header__button');
	
	if(w<767.98){
		if (!headerLink.hasClass('done')) {
			headerLink.addClass('done').appendTo(headerMenu);
		}
	}
	else{
		if (headerLink.hasClass('done')) {
			headerLink.removeClass('done').prependTo($('.header__top'));
		}
	}

	if(w<767.98){
		if (!$('.menu__list').hasClass('done')) {
			$('.menu__list').addClass('done').appendTo(headerMenu);
		}
	}
	else{
		if ($('.menu__list').hasClass('done')) {
			$('.menu__list').removeClass('done').appendTo($('.menu__body'));
		}
	}
}

function adaptiveFunction() {
	let w=$(window).outerWidth();
	let h=$(window).outerHeight();
	adaptiveHeader(w,h);
}
adaptiveFunction();