//==============================================================================================================
//WebP
function testWebP(callback) {
  let webP = new Image();
  webP.onload = webP.onerror = function () {
    callback(webP.height == 2);
  };
  webP.src = "data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA";
}
testWebP(function (support) {
  if (support == true) {
    document.querySelector('html').classList.add('_webp');
  } else {
    document.querySelector('html').classList.add('_no-webp');
  }
});
//==============================================================================================================
//Determine PC or Touchscreen
let isMobile = { 
  Android: function () {
    return navigator.userAgent.match(/Android/i); 
  }, 
  BlackBerry: function () { 
    return navigator.userAgent.match(/BlackBerry/i); 
  }, 
  iOS: function () { 
    return navigator.userAgent.match(/iPhone|iPad|iPod/i); 
  }, 
  Opera: function () { 
    return navigator.userAgent.match(/Opera Mini/i); 
  }, 
  Windows: function () { 
    return navigator.userAgent.match(/IEMobile/i); 
  }, 
  any: function () { 
    return (
      isMobile.Android() || 
      isMobile.BlackBerry() || 
      isMobile.iOS() || 
      isMobile.Opera() || 
      isMobile.Windows()); 
  } 
};
//..and add class
if (isMobile.any()) {
  document.querySelector('body').classList.add('_touchscreen');

  //if touchscreen has arrows show them (for sub-list)
  let menuArrows = document.querySelectorAll('.menu__arrow');
  if (menuArrows.length > 0) {
    for (let index = 0; index < menuArrows.length; index++) {
      const menuArrow = menuArrows[index];
        menuArrow.addEventListener("click", function(e) {
          //..for 1st arrow: add onclick class "_active" for parent element
          menuArrow.parentElement.classList.toggle('_active');
          //..for 1st arrow: add onclick class "_active"
          menuArrow.classList.toggle('_active');
        });
    }
  }
} else {
  document.querySelector('body').classList.add('_pc');
}
//==============================================================================================================
//Adaptive items
@@include('_responsive.js')
//==============================================================================================================
//Add classes on Burger click
let iconMenu = document.querySelector('.icon-menu');
let menuBody = document.querySelector('.menu__body');

if (iconMenu != null) {
  iconMenu.addEventListener("click", function (e) {
    iconMenu.classList.toggle('_active');
    menuBody.classList.toggle('_active');
    document.body.classList.toggle('_lock');
  });
}
//==============================================================================================================
//Remove classes on adaptive menu item click
let menuLinks = document.querySelectorAll('._goto-block');

menuLinks.forEach(menuLink => {
  menuLink.addEventListener("click", onMenuLinkClick);
});

function onMenuLinkClick() {
  document.body.classList.remove('_lock');
  iconMenu.classList.remove('_active');
  menuBody.classList.remove('_active');
}
//==============================================================================================================
//Slick (regular settings)
@@include('slick.min.js')
//==============================================================================================================
//Slick
$(document).ready(function(){
  $('.slider__body').slick({
    dots: true,
    slidesToShow: 4,
    infinite: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 590,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  });
});
//=================================
//Slick for popup
$(document).ready(function(){
  $('.popup-slider__body').slick({
    dots: false,
    slidesToShow: 1,
    infinite: false, 
  });
});
//==============================================================================================================
//Spollers: class on click with jQuery
$(document).ready(function(){
  $('.spollers__title').click(function(event){
    if($('.spollers').hasClass('_one')){
      $('.spollers__title').not($(this)).removeClass('_active');
      $('.spollers__body').not($(this).next()).slideUp(300);
    }
  $(this).toggleClass('_active').next().slideToggle(300);
  });
});
//==============================================================================================================
//Popups
//Open-popup
let popupLinks = document.querySelectorAll('._popup-link'); 
let popups = document.querySelector('.popup');

popupLinks.forEach(popupLink => {
  popupLink.addEventListener("click", popupOpen);
});

function popupOpen() {
  document.body.classList.toggle('_lock');
  popups.classList.toggle('_active');
}
//=================================
//Close-popup
let closingElements = document.querySelectorAll('._close'); 

closingElements.forEach(closeElement => {
  closeElement.addEventListener("click", popupClose);
});

popups.addEventListener("click", function (e) {
  if (!e.target.closest('.popup__body')) {
    popupClose(e.target.closest('.popup'));
  }
});

function popupClose() {
  document.body.classList.remove('_lock');
  popups.classList.remove('_active');
}
//==============================================================================================================
//Animation on scroll
const animItems = document.querySelectorAll('._animation');

if (animItems.length > 0) {
  window.addEventListener('scroll', animOnScroll);
  function animOnScroll() {
    for (let index = 0; index < animItems.length; index++) {
      const animItem = animItems[index];
      //defined the item height
      const animItemHeight = animItem.offsetHeight;
      //defined the item position
      const animItemOffset = offset(animItem).top;
      //animation start coefficient
      const animStart = 4;

      //the moment the animation starts
      let animItemPoint = window.innerHeight - animItemHeight / animStart;
      //
      if (animItemHeight > window.innerHeight) {
        animItemPoint = window.innerHeight - window.innerHeight / animStart;
      }
      //If you scrolled to the 1/4 of item height or window height..
      if ((pageYOffset > animItemOffset - animItemPoint) && pageYOffset < (animItemOffset + animItemHeight)) {
        animItem.classList.add('_active');
      } else {
        //._one-anim to hide animation repeat
        if (!animItem.classList.contains('_one-anim')) {
          animItem.classList.remove('_active');
        }
      }
    }
  }

  function offset(el) {
    const rect = el.getBoundingClientRect(),
      scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
      scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
  }

  //if item is on the header
  setTimeout(() => {
    animOnScroll();
  }, 500);
}
//==============================================================================================================
